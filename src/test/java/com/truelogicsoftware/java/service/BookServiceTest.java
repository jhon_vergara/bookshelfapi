package com.truelogicsoftware.java.service;

import com.truelogicsoftware.java.database.BookDAO;
import com.truelogicsoftware.java.database.public_.tables.records.BookRecord;
import com.truelogicsoftware.java.model.BookDTO;
import com.truelogicsoftware.java.model.ResultDTO;
import com.truelogicsoftware.java.resources.BookResource;
import org.jooq.Record9;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BookServiceTest {

    private final BookDAO dao = mock(BookDAO.class);
    private final BookService service = new BookService(dao, "yyyy-MM-dd'T'hh:mm:ss.Z'Z'");

    @Test
    public void getBooksShouldReturnExpectedData() {

        List<Record9<String,String,String,Timestamp,String,Integer,String,Integer,String>> list = new ArrayList<>();
        when(dao.getBooks(any(), any())).thenReturn(list.stream());
        final List<BookDTO> result = service.getBooks("","");

        assertNotNull(result);
    }

    @Test
    public void performHealthCheckOperatesPerfectly() {

        BookRecord[] records = new BookRecord[3];
        when(dao.getBooks()).thenReturn(records);
        final String result = service.performHealthCheck();

        assertNull(result);
    }

    @Test
    public void performHealthCheckCheckException() {

        UnsupportedOperationException exception = new UnsupportedOperationException("error");
        exception.initCause(new IllegalArgumentException("illegal"));
        when(dao.getBooks()).thenThrow(exception);
        final String result = service.performHealthCheck();
        assertNotNull(result);
    }

}