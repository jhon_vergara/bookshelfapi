package com.truelogicsoftware.java.resources;

import com.truelogicsoftware.java.model.BookDTO;
import com.truelogicsoftware.java.model.ResultDTO;
import com.truelogicsoftware.java.service.BookService;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BookResourceTest {
    private final BookService service = mock(BookService.class);
    private final BookResource resource = new BookResource(service);

    @Test
    public void filterBooksTestShouldReturnValues() {

        List<BookDTO> bookDTOList = new ArrayList<>();
        bookDTOList.add(new BookDTO());
        bookDTOList.add(new BookDTO());
        bookDTOList.add(new BookDTO());

        when(service.getBooks(any(String.class), any(String.class))).thenReturn(bookDTOList);
        final ResultDTO<List<BookDTO>> result = resource.filterBooks("java", "programming");

        assertNotNull(result);
        assertNotNull(result.getData());
        assertEquals(3, result.getData().size());
    }

    @Test
    public void filterBooksTestShouldNotReturnValues() {

        when(service.getBooks(any(String.class), any(String.class))).thenReturn(null);

        final ResultDTO<List<BookDTO>> result = resource.filterBooks("java", "programming");

        assertNotNull(result);
        assertNull(result.getData());
    }

}