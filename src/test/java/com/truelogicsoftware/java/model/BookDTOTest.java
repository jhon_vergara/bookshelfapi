package com.truelogicsoftware.java.model;

import org.junit.Test;
import org.meanbean.test.BeanTester;

public class BookDTOTest {

    private BeanTester tester;

    @Test
    public void testBookDTO() {
        tester = new BeanTester();
        tester.testBean(BookDTO.class);
    }
}