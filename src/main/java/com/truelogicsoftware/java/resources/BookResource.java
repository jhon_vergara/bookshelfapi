package com.truelogicsoftware.java.resources;

import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import com.truelogicsoftware.java.model.BookDTO;
import com.truelogicsoftware.java.model.ResultDTO;
import com.truelogicsoftware.java.service.BookService;
import java.util.List;


@Path("/books")
@Produces(MediaType.APPLICATION_JSON)
public class BookResource {

    private final BookService bookService;

    public BookResource(BookService bookService) {
        this.bookService = bookService;
    }

    @GET
    @Timed
    public ResultDTO<List<BookDTO>> filterBooks(
            @QueryParam("title") final String title,
            @QueryParam("description") final String description){
        return new ResultDTO<>(bookService.getBooks(title, description));
    }

}
