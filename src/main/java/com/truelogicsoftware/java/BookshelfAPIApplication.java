package com.truelogicsoftware.java;

import com.bendb.dropwizard.jooq.JooqBundle;
import com.bendb.dropwizard.jooq.JooqFactory;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.jooq.Configuration;

import com.truelogicsoftware.java.database.BookDAO;
import com.truelogicsoftware.java.health.BookshelfAPIHealthCheck;
import com.truelogicsoftware.java.resources.BookResource;
import com.truelogicsoftware.java.service.BookService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class BookshelfAPIApplication extends Application<BookshelfAPIConfiguration> {


    private static final String APPLICATION = "BookshelfAPI";
    private static final String BOOK_SERVICE = "Book service";
    private JooqBundle<BookshelfAPIConfiguration> jooqBundle;

    public static void main(final String[] args) throws Exception {
        new BookshelfAPIApplication().run(args);
    }

    @Override
    public String getName() {
        return APPLICATION;
    }

    @Override
    public void initialize(final Bootstrap<BookshelfAPIConfiguration> bootstrap) {

        // Datasource configuration
        bootstrap.addBundle(new MigrationsBundle<BookshelfAPIConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(BookshelfAPIConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });

        jooqBundle = new JooqBundle<BookshelfAPIConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(BookshelfAPIConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }

            @Override
            public JooqFactory getJooqFactory(BookshelfAPIConfiguration configuration) {
                return configuration.getJooqFactory();
            }
        };
        bootstrap.addBundle(jooqBundle);
    }

    @Override
    public void run(final BookshelfAPIConfiguration configuration,
                    final Environment environment) {

        Configuration jooqConfiguration = jooqBundle.getConfiguration();
        BookService bookService = new BookService(new BookDAO(jooqConfiguration), configuration.getDateFormat());

        // Registering Health Check
        BookshelfAPIHealthCheck healthCheck = new BookshelfAPIHealthCheck(bookService);
        environment.healthChecks().register(BOOK_SERVICE, healthCheck);

        //Setting up DateFormat
        DateFormat eventDateFormat = new SimpleDateFormat(configuration.getDateFormat());
        environment.getObjectMapper().setDateFormat(eventDateFormat);

        //Registering Resources
        environment.jersey().register(new BookResource(bookService));
    }

}
