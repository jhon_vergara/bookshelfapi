package com.truelogicsoftware.java.service;

import com.truelogicsoftware.java.database.BookDAO;
import com.truelogicsoftware.java.model.BookDTO;
import org.jooq.Record9;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BookService {

    private static final String UNEXPECTED_ERROR = "An unexpected error occurred while processing book: ";

    private final BookDAO bookDAO;
    private final String dateFormat;

    public BookService(@NotNull final BookDAO bookDAO, @NotNull final String dateFormat) {
        this.bookDAO = bookDAO;
        this.dateFormat = dateFormat;
    }

    public List<BookDTO> getBooks(String title, String description) {
        return mapping(bookDAO.getBooks(title, description));
    }

    public String performHealthCheck() {
        try {
            bookDAO.getBooks();
        } catch (Exception ex) {
            return UNEXPECTED_ERROR + ex.getCause().getLocalizedMessage();
        }
        return null;
    }

    private List<BookDTO> mapping(Stream<Record9<String, String, String, Timestamp, String, Integer, String, Integer, String>> recordStream) {
        return recordStream
                .collect(Collectors.groupingBy(
                        BookService.this::mapping,
                        LinkedHashMap::new,
                        Collectors.mapping(
                                Record9::value9,
                                Collectors.toList()
                        )
                ))
                // Moves the List<String> Entry.value into each Book.author key
                .entrySet()
                .stream()
                .map(authorMap -> {
                    BookDTO bookDTO = authorMap.getKey();
                    if(!Optional.ofNullable(bookDTO.getAuthors()).isPresent()){
                        bookDTO.setAuthors(new ArrayList<>());
                    }
                    bookDTO.getAuthors().addAll(authorMap.getValue());
                    return authorMap.getKey();
                })
                .collect(Collectors.toList());
    }

    private BookDTO mapping(Record9<String, String, String, Timestamp, String, Integer, String, Integer, String> record) {
        DateFormat localDateFormat = new SimpleDateFormat(this.dateFormat);

        BookDTO bookDTO = new BookDTO();
        bookDTO.setIsbn(record.value1());
        bookDTO.setTitle(record.value2());
        bookDTO.setSubtitle(record.value3());
        try {
            bookDTO.setPublished(localDateFormat.parse(localDateFormat.format(record.value4())));
        } catch (ParseException e) {
            bookDTO.setPublished(record.value4());
        }
        bookDTO.setPublisher(record.value5());
        bookDTO.setPages(record.value6());
        bookDTO.setDescription(record.value7());
        bookDTO.setInstock((record.value8() == 1));
        return bookDTO;
    }

}
