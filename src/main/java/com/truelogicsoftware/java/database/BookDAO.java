package com.truelogicsoftware.java.database;

import com.truelogicsoftware.java.database.public_.tables.records.BookRecord;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.Record9;
import org.jooq.impl.DSL;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.stream.Stream;

import static com.truelogicsoftware.java.database.public_.Tables.AUTHOR;
import static com.truelogicsoftware.java.database.public_.tables.Book.BOOK;


public class BookDAO {
    private static final String LIKE = "%";

    private Configuration jooqConfig;

    public BookDAO(Configuration jooqConfig) {
        this.jooqConfig = jooqConfig;
    }

    public BookRecord[] getBooks() {
        return this.getDatabase()
                .selectFrom(BOOK)
                .fetchArray();
    }

    public Stream<Record9<String,String,String,Timestamp,String,Integer,String,Integer,String>> getBooks(
            @NotNull final String title,
            @NotNull final String desc){
        String titleQuery = (title != null && !title.trim().equals(""))? LIKE + title.trim() + LIKE : "";
        String descriptionQuery = (desc != null && !desc.trim().equals(""))? LIKE + desc.trim() + LIKE : "";
       return this.getDatabase()
                .select(
                        BOOK.ISBN,
                        BOOK.TITLE,
                        BOOK.SUBTITLE,
                        BOOK.PUBLISHED,
                        BOOK.PUBLISHER,
                        BOOK.PAGES,
                        BOOK.DESCRIPTION,
                        BOOK.INSTOCK,
                        AUTHOR.NAME)
                .from(BOOK)
                .innerJoin(AUTHOR)
                .on(BOOK.ISBN.eq(AUTHOR.BOOK_ID))
                .where(BOOK.TITLE.like(titleQuery))
                .or(BOOK.DESCRIPTION.like(descriptionQuery))
                .fetch()
                .stream();
    }

    private DSLContext getDatabase(){
        return DSL.using(jooqConfig);
    }
}
