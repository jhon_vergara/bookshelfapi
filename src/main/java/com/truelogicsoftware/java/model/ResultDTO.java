package com.truelogicsoftware.java.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultDTO<T> {
    private static final String DATA_TITLE = "bookshelf";

    @JsonProperty(DATA_TITLE)
    private T data;

    public ResultDTO() {
        // Jackson deserialization
    }

    public ResultDTO(T data) {
        this.data = data;
    }

    @JsonProperty
    public T getData() {
        return data;
    }
}
