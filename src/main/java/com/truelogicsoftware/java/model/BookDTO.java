package com.truelogicsoftware.java.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class BookDTO {

    private String isbn;
    private String title;
    private String subtitle;
    private List<String> authors;
    private Date published;
    private String publisher;
    private int pages;
    private String description;
    private boolean instock;

}
