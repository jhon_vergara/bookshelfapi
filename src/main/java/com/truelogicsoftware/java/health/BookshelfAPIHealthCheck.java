package com.truelogicsoftware.java.health;

import com.codahale.metrics.health.HealthCheck;
import com.truelogicsoftware.java.service.BookService;


public class BookshelfAPIHealthCheck extends HealthCheck {
    private static final String HEALTHY = "The Bookshelf API is healthy!";
    private static final String UNHEALTHY = "The Bookshelf API is not healthy! ";
    private static final String MESSAGE_PLACEHOLDER = "{}";

    private final BookService bookService;

    public BookshelfAPIHealthCheck(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public Result check() {
        String dbStatus = bookService.performHealthCheck();

        if (dbStatus == null) {
            return Result.healthy(HEALTHY);
        } else {
            return Result.unhealthy(UNHEALTHY + MESSAGE_PLACEHOLDER, dbStatus);
        }
    }
}