package com.truelogicsoftware.java;

import com.bendb.dropwizard.jooq.JooqFactory;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import org.hibernate.validator.constraints.*;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class BookshelfAPIConfiguration extends Configuration {

    private static final String DATABASE = "database";
    private static final String JOOQ = "jooq";

    @NotEmpty
    private String version;

    @NotEmpty
    private String dateFormat;

    @Valid
    @NotNull
    @JsonProperty(DATABASE)
    private DataSourceFactory dataSourceFactory = new DataSourceFactory();

    @Valid
    @NotNull
    @JsonProperty(JOOQ)
    private JooqFactory jooqFactory = new JooqFactory();

    @JsonProperty
    public String getDateFormat() {
        return dateFormat;
    }

    @JsonProperty
    public String getVersion() {
        return version;
    }

    public DataSourceFactory getDataSourceFactory() {
        return dataSourceFactory;
    }

    public JooqFactory getJooqFactory() {
        return jooqFactory;
    }
}
