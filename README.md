#Easiest way to run the project in production:
gradle buildDockerImage
gcloud docker -- push eu.gcr.io/bookshelf-api-production/bookshelf-api:production-latest
cd kubernates/production/
kubectl create -f bookshelf-api-ingress.yml
kubectl create -f bookshelf-api-deployment.yml
kubectl create -f bookshelf-api-service.yml
kubectl get services

# Set docker environment to minikube
eval $(minikube docker-env)

# Build project and container
gradle clean build buildDockerImage

#Check docker images
docker images

#Access to kubernates file descriptors
cd kubernates/local/

#Create kubernates deployment and service
kubectl create -f bookshelf-api-deployment.yml
kubectl create -f bookshelf-api-service.yml

#Check pods created
kubectl get pods

#Check services created
kubectl get services

#Access to the service
minikube service bookshelf-api

#Additional commands
kubectl cluster-info


#Google cloud
gcloud auth login
gcloud components update
gcloud components install kubectl

gradle pushDockerImage -Penv=production
gcloud docker -- push eu.gcr.io/bookshelf-api-production/bookshelf-api:production-latest

#Deploy in production
cd kubernates/production/
kubectl create -f bookshelf-api-service.yml